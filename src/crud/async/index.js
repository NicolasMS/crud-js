
const fetchData = require('../utils/fetch');
const APIENDPOINT = "https://jsonplaceholder.typicode.com/posts/";

// LLamado a la función que devuelve los datos del end point
const endPoint = async (urlApi) => {
    try {
        const resolveData = await fetchData(urlApi);
        console.log(resolveData);
    } catch (err) {
        console.error(err);
    }
};
// Ejecución de la función asíncrona envía la url a FETCH
endPoint(APIENDPOINT);
